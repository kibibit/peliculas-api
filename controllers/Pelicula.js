const Peliculas = require('../models/Pelicula')

exports.getAll = async(req,res,next) =>{
    try{
        productos = await Peliculas.find({})
        if (productos.length == 0) {
            return res.status(404).json({ message: 'No hay peliculas en el catalogo'});
        }
        res.status(200).json(productos);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
    next();
}

exports.Create = async(req, res, next) => {  
    try{
        const producto = await Peliculas.create(req.body);
        res.status(200).json(producto);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
    next();
}

exports.getOne = async(req,res,next) => {
    try{
        //const producto = Peliculas.findById(req.params.productoId)
        const producto = await Peliculas.findOne({ _id: req.params.peliculaId});
        if(producto == null){
            return res.status(404).send({error: 'No existe esta pelicula'});
        }
        res.status(200).json(producto);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
    next();
}

exports.Update = async(req, res, next) => {
    try{
        const producto = await Peliculas.findByIdAndUpdate(req.params.peliculaId, {
            $set: req.body
        }, { new: true });
        if(producto == null) return res.status(404).send({error: 'No existe esta pelicula'});
        res.status(200).json(producto);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
}

exports.Delete = async(req, res, next) => {
    try{
        const respuesta = await Productos.findByIdAndRemove(req.params.peliculaId);
        res.status(200).json(respuesta);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
}

exports.NotSupported = (req, res, next) => {
    return res.status(403).json({message: 'Operacion no soportada.'});
}
