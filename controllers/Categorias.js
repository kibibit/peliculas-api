const Categorias = require('../models/Categorias')

exports.getAll = async(req,res,next) =>{
    try{
        productos = await Categorias.find({})
        if (productos.length == 0) {
            return res.status(404).json({ message: 'No hay categorias de peliculas'});
        }
        res.status(200).json(productos);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
    next();
}

exports.Create = async(req, res, next) => {  
    try{
        const producto = await Categorias.create(req.body);
        res.status(200).json(producto);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
    next();
}

exports.getOne = async(req,res,next) => {
    try{
        //const producto = Peliculas.findById(req.params.productoId)
        const producto = await Categorias.findOne({ _id: req.params.categoriaId});
        if(producto == null){
            return res.status(404).send({error: 'No existe esta categoria'});
        }
        res.status(200).json(producto);
    }catch(eer){
        return res.status(500).json({message: eer.message});
    }
    next();
}

exports.NotSupported = (req, res, next) => {
    return res.status(403).json({message: 'Operacion no soportada.'});
}
