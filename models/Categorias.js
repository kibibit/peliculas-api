const mongoose = require('mongoose')

const categoriaSchema = mongoose.Schema({
    categoria: {
        type: String
    }
})

module.exports = mongoose.model('Categorias',categoriaSchema);