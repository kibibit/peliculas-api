const mongoose = require('mongoose')

const peliculaSchema = mongoose.Schema({
    caratula: {
        type: String,
        required: true
    },
    titulo: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    duracion: {
        type: Number,
        required: true
    },
    trailer: {
        type: String,
        required: true
    },
    estreno: {
        type: Date,
        required: true
    },
    categorias: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Categorias'
        }
    ]
})

module.exports = mongoose.model('Pelicula',peliculaSchema)