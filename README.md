# Api de peliculas realizada con express y mongoDB

Api hecha con express y mongodb, utiliza variables de entorno y estara alojada en heroku. 

## Pasos a seguir
```bash
# instalar dependencias
$ npm install

# correr en desarrollo
$ npm run test

# correr en produccion
$ npm run start

```
