const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
require('dotenv').config();

const port = process.env.PORT || 3000;
const hostname = process.env.HOST || '0.0.0.0';

mongoose.connect(process.env.DB_URL, { useNewUrlParser: true , useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('connected to database'));
mongoose.set('useCreateIndex',true)

app.use(express.json());
app.use(cors());
app.use(morgan('dev'));
app.use(helmet());

const apiRouter = require('./routes')
app.use(apiRouter)

app.get('/', function (req, res) {
    res.send('hello')
});

app.listen(port, hostname, ()=> {
  console.log(`Server running at http://${hostname}:${port}`);
});