const express = require('express');
const router = express.Router();
const Categoria = require('../controllers/Categorias');

router.route('/')
.get(Categoria.getAll)
.post(Categoria.Create)
.put(Categoria.NotSupported)
.delete(Categoria.NotSupported)

router.route('/:categoriaId')
.get(Categoria.getOne)
.post(Categoria.NotSupported)
.put(Categoria.NotSupported)
.delete(Categoria.NotSupported);

module.exports = router