const express = require('express');
const router = express.Router();
const Lista = require('../controllers/Lista');

router.route('/')
.get(Lista.GetAll)
.post(Lista.Create)

router.route('/:listaId')
.get(Lista.GetOne)
.post(Lista.NotSupported)

module.exports = router