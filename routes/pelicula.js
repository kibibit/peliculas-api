const express = require('express');
const router = express.Router();
const Pelicula = require('../controllers/Pelicula');

router.route('/')
.get(Pelicula.getAll)
.post(Pelicula.Create)
.put(Pelicula.NotSupported)
.delete(Pelicula.NotSupported)

router.route('/:peliculaId')
.get(Pelicula.getOne)
.post(Pelicula.NotSupported)
.put(Pelicula.Update)
.delete(Pelicula.Delete);

module.exports = router