const express = require('express');
const routes = express.Router();

const peliculaRouter = require('./pelicula');
const categoriaRouter = require('./categorias');

routes.use('/pelicula',peliculaRouter);
routes.use('/categoria',categoriaRouter);

module.exports = routes